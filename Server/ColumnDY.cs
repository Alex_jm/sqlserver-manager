﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Maticsoft.Model;
using System.Collections;
using Maticsoft.Common;

namespace Server
{
    public partial class ColumnDY : Office2007Form
    {
        public ColumnDY()
        {
            InitializeComponent();
        }
        public ArrayList ar = new ArrayList();
        public ArrayList ar1 = new ArrayList();
        //窗体加载事件
        private void SetColumns_Load(object sender, EventArgs e)
        {
            listnew.Items.Clear();
            listold.Items.Clear();
            ListBing();
        }
        //数据绑定
        public void ListBing()
        {
            try
            {
                foreach (string s in ar1)
                {
                    listold.Items.Add(s);
                }
                listold.SelectedItem = null;
                foreach (string s in ar)
                {
                    listnew.Items.Add(s);
                }
                listnew.SelectedItem = null;
            }
            catch { }
        }
        //添加事件
        private void qu_Click(object sender, EventArgs e)
        {
            try
            {
                while (listold.SelectedItems.Count > 0)
                {
                    int j = 0;
                    for (int i = 0; i < dgvImport.Rows.Count; i++)
                    {
                        if (dgvImport.Rows[i].Cells["sjkl"].Value == null || dgvImport.Rows[i].Cells["sjkl"].Value.ToString().Trim().Equals(""))
                        {
                            j = i;
                            break;
                        }
                    }
                    if (j == dgvImport.Rows.Count - 1) dgvImport.Rows.Add();
                    dgvImport.Rows[j].Cells["sjkl"].Value = listold.SelectedItems[0].ToString();
                    listold.Items.Remove(listold.SelectedItems[0]);
                }
            }
            catch { }
        }
        //添加所有事件
        private void quall_Click(object sender, EventArgs e)
        {
            try
            {
                while (listold.Items.Count > 0)
                {
                    int j = 0;
                    for (int i = 0; i < dgvImport.Rows.Count; i++)
                    {
                        if (dgvImport.Rows[i].Cells["sjkl"].Value == null || dgvImport.Rows[i].Cells["sjkl"].Value.ToString().Trim().Equals(""))
                        {
                            j = i;
                            break;
                        }
                    }
                    if (j == dgvImport.Rows.Count - 1) dgvImport.Rows.Add();
                    dgvImport.Rows[j].Cells["sjkl"].Value = listold.Items[0].ToString();
                    listold.Items.Remove(listold.Items[0]);
                }
            }
            catch { }
        }
        //添加事件
        private void lai_Click(object sender, EventArgs e)
        {
            try
            {
                while (listnew.SelectedItems.Count > 0)
                {
                    int j = 0;
                    for (int i = 0; i < dgvImport.Rows.Count; i++)
                    {
                        if (dgvImport.Rows[i].Cells["drl"].Value == null || dgvImport.Rows[i].Cells["drl"].Value.ToString().Trim().Equals(""))
                        {
                            j = i;
                            break;
                        }
                    }
                    if (j == dgvImport.Rows.Count - 1) dgvImport.Rows.Add();
                    dgvImport.Rows[j].Cells["drl"].Value = listnew.SelectedItems[0].ToString();
                    listnew.Items.Remove(listnew.SelectedItems[0]);
                }
            }
            catch { }
        }
        //添加所有事件
        private void laiall_Click(object sender, EventArgs e)
        {
            try
            {
                while (listnew.Items.Count > 0)
                {
                    int j = 0;
                    for (int i = 0; i < dgvImport.Rows.Count; i++)
                    {
                        if (dgvImport.Rows[i].Cells["drl"].Value == null || dgvImport.Rows[i].Cells["drl"].Value.ToString().Trim().Equals(""))
                        {
                            j = i;
                            break;
                        }
                    }
                    if (j == dgvImport.Rows.Count - 1) dgvImport.Rows.Add();
                    dgvImport.Rows[j].Cells["drl"].Value = listnew.Items[0].ToString();
                    listnew.Items.Remove(listnew.Items[0]);
                }
            }
            catch { }
        }
        //保存事件
        private void buttonX1_Click(object sender, EventArgs e)
        {
            try
            {
                Servers.old_db.Clear();
                Servers.new_db.Clear();
                foreach (DataGridViewRow r in dgvImport.Rows)
                {
                    if (r.Cells["sjkl"].Value != null && r.Cells["drl"].Value != null &&
                        !r.Cells["sjkl"].Value.ToString().Trim().Equals("") && !r.Cells["drl"].Value.ToString().Trim().Equals(""))
                    {
                        string s = "false";
                        if (r.Cells["cbo"].Value != null && r.Cells["cbo"].Value.Equals(true)) s = "true";
                        Hashtable hb1 = new Hashtable();
                        Hashtable hb2 = new Hashtable();
                        hb1.Add(r.Cells["sjkl"].Value.ToString().Trim(), s);
                        hb2.Add(r.Cells["drl"].Value.ToString().Trim(), s);
                        Servers.old_db.Add(r.Index, hb1);
                        Servers.new_db.Add(r.Index, hb2);
                    }
                }
                this.Close();
            }
            catch { }
        }
        //重置事件
        private void buttonX2_Click(object sender, EventArgs e)
        {
            try
            {
                listnew.Items.Clear();
                listold.Items.Clear();
                ListBing();
                dgvImport.Rows.Clear();
            }
            catch { }
        }
        //删除操作
        private void dgvImport_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    //删除
                    if (dgvImport.Columns[e.ColumnIndex].Name.Equals("sc"))
                    {
                        if (dgvImport.Rows.Count > 0)
                        {
                            if (dgvImport.Rows[e.RowIndex].Cells["drl"].Value != null && !dgvImport.Rows[e.RowIndex].Cells["drl"].Value.ToString().Equals("")) listnew.Items.Add(dgvImport.Rows[e.RowIndex].Cells["drl"].Value.ToString());
                            if (dgvImport.Rows[e.RowIndex].Cells["sjkl"].Value != null && !dgvImport.Rows[e.RowIndex].Cells["sjkl"].Value.ToString().Equals("")) listold.Items.Add(dgvImport.Rows[e.RowIndex].Cells["sjkl"].Value.ToString());
                            dgvImport.Rows.RemoveAt(e.RowIndex);
                        }
                    }
                }
            }
            catch { }
        }
    }
}
