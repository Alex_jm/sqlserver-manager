﻿namespace Server
{
    partial class ColumnDY
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ColumnDY));
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.listold = new System.Windows.Forms.ListBox();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.listnew = new System.Windows.Forms.ListBox();
            this.qu = new DevComponents.DotNetBar.ButtonX();
            this.quall = new DevComponents.DotNetBar.ButtonX();
            this.lai = new DevComponents.DotNetBar.ButtonX();
            this.laiall = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.dgvImport = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.cbo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sjkl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.drl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sc = new System.Windows.Forms.DataGridViewLinkColumn();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvImport)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.listold);
            this.groupPanel1.Location = new System.Drawing.Point(0, 0);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(110, 202);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "数据库列";
            // 
            // listold
            // 
            this.listold.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listold.FormattingEnabled = true;
            this.listold.ItemHeight = 12;
            this.listold.Location = new System.Drawing.Point(0, 0);
            this.listold.Name = "listold";
            this.listold.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listold.Size = new System.Drawing.Size(104, 178);
            this.listold.TabIndex = 1;
            // 
            // groupPanel2
            // 
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.listnew);
            this.groupPanel2.Location = new System.Drawing.Point(0, 205);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(110, 204);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel2.TabIndex = 1;
            this.groupPanel2.Text = "导入列";
            // 
            // listnew
            // 
            this.listnew.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listnew.FormattingEnabled = true;
            this.listnew.ItemHeight = 12;
            this.listnew.Location = new System.Drawing.Point(0, 0);
            this.listnew.Name = "listnew";
            this.listnew.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listnew.Size = new System.Drawing.Size(104, 180);
            this.listnew.TabIndex = 0;
            // 
            // qu
            // 
            this.qu.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.qu.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.qu.Location = new System.Drawing.Point(115, 61);
            this.qu.Name = "qu";
            this.qu.Size = new System.Drawing.Size(50, 26);
            this.qu.TabIndex = 2;
            this.qu.Text = "<font color=\"#758C48\" size=\"16\">&gt;</font>";
            this.qu.Click += new System.EventHandler(this.qu_Click);
            // 
            // quall
            // 
            this.quall.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.quall.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.quall.Location = new System.Drawing.Point(115, 108);
            this.quall.Name = "quall";
            this.quall.Size = new System.Drawing.Size(50, 26);
            this.quall.TabIndex = 3;
            this.quall.Text = "<font color=\"#758C48\" size=\"16\">&gt;&gt;</font>";
            this.quall.Click += new System.EventHandler(this.quall_Click);
            // 
            // lai
            // 
            this.lai.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.lai.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.lai.Location = new System.Drawing.Point(115, 278);
            this.lai.Name = "lai";
            this.lai.Size = new System.Drawing.Size(50, 25);
            this.lai.TabIndex = 4;
            this.lai.Text = "<font color=\"#758C48\" size=\"16\">\r\n&gt;\r\n</font>";
            this.lai.Click += new System.EventHandler(this.lai_Click);
            // 
            // laiall
            // 
            this.laiall.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.laiall.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.laiall.Location = new System.Drawing.Point(114, 319);
            this.laiall.Name = "laiall";
            this.laiall.Size = new System.Drawing.Size(50, 27);
            this.laiall.TabIndex = 5;
            this.laiall.Text = "<font color=\"#758C48\" size=\"16\">&gt;&gt;</font>";
            this.laiall.Click += new System.EventHandler(this.laiall_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(114, 369);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(50, 31);
            this.buttonX1.TabIndex = 6;
            this.buttonX1.Text = "保存";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // dgvImport
            // 
            this.dgvImport.AllowUserToDeleteRows = false;
            this.dgvImport.AllowUserToOrderColumns = true;
            this.dgvImport.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgvImport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvImport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cbo,
            this.sjkl,
            this.drl,
            this.sc});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvImport.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvImport.Dock = System.Windows.Forms.DockStyle.Right;
            this.dgvImport.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvImport.HighlightSelectedColumnHeaders = false;
            this.dgvImport.Location = new System.Drawing.Point(170, 0);
            this.dgvImport.Name = "dgvImport";
            this.dgvImport.RowTemplate.Height = 23;
            this.dgvImport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvImport.Size = new System.Drawing.Size(390, 419);
            this.dgvImport.TabIndex = 8;
            this.dgvImport.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvImport_CellContentClick);
            // 
            // cbo
            // 
            this.cbo.HeaderText = "标识";
            this.cbo.Name = "cbo";
            this.cbo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cbo.Width = 40;
            // 
            // sjkl
            // 
            this.sjkl.HeaderText = "数据列";
            this.sjkl.Name = "sjkl";
            this.sjkl.Width = 130;
            // 
            // drl
            // 
            this.drl.HeaderText = "导入列";
            this.drl.Name = "drl";
            this.drl.Width = 130;
            // 
            // sc
            // 
            this.sc.HeaderText = "删除";
            this.sc.Name = "sc";
            this.sc.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.sc.Text = "删除";
            this.sc.UseColumnTextForLinkValue = true;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(115, 159);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(50, 31);
            this.buttonX2.TabIndex = 9;
            this.buttonX2.Text = "重置";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // ColumnDY
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 419);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.dgvImport);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.laiall);
            this.Controls.Add(this.lai);
            this.Controls.Add(this.quall);
            this.Controls.Add(this.qu);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ColumnDY";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "选择标识列";
            this.Load += new System.EventHandler(this.SetColumns_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvImport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX qu;
        private System.Windows.Forms.ListBox listold;
        private System.Windows.Forms.ListBox listnew;
        private DevComponents.DotNetBar.ButtonX quall;
        private DevComponents.DotNetBar.ButtonX lai;
        private DevComponents.DotNetBar.ButtonX laiall;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvImport;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbo;
        private System.Windows.Forms.DataGridViewTextBoxColumn sjkl;
        private System.Windows.Forms.DataGridViewTextBoxColumn drl;
        private System.Windows.Forms.DataGridViewLinkColumn sc;
        private DevComponents.DotNetBar.ButtonX buttonX2;
    }
}