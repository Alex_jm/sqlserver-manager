﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using Aspose.Cells;
using System.Web;
using System.Drawing;

namespace ExportFile
{
    public class port
    {
        //导出操作
        public static Boolean export(List<export> list, String path)
        {
            try
            {
                if (list.Count > 0)
                {
                    Workbook wb = new Workbook();
                    int intSeet = 0;

                    #region "表格样式"
                    wb.Styles.Add();
                    wb.Styles[0].VerticalAlignment = TextAlignmentType.Center;
                    wb.Styles[0].HorizontalAlignment = TextAlignmentType.Center;
                    wb.Styles[0].Font.Color = ColorTranslator.FromHtml("#000000");
                    wb.Styles[0].Font.Size = 10;
                    wb.Styles[0].Font.Name = "宋体";
                    wb.Styles[0].Font.IsBold = true;
                    wb.Styles[0].Pattern = BackgroundType.Solid;
                    wb.Styles[0].ForegroundColor = ColorTranslator.FromHtml("#CCFFFF");
                    wb.Styles[0].Borders[BorderType.TopBorder].Color = Color.Black;
                    wb.Styles[0].Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles[0].Borders[BorderType.BottomBorder].Color = Color.Black;
                    wb.Styles[0].Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles[0].Borders[BorderType.LeftBorder].Color = Color.Black;
                    wb.Styles[0].Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles[0].Borders[BorderType.RightBorder].Color = Color.Black;
                    wb.Styles[0].Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles.Add();
                    wb.Styles[1].VerticalAlignment = TextAlignmentType.Center;
                    wb.Styles[1].HorizontalAlignment = TextAlignmentType.Center;
                    wb.Styles[1].Font.Color = ColorTranslator.FromHtml("#000000");
                    wb.Styles[1].Font.Size = 10;
                    wb.Styles[1].Font.Name = "宋体";
                    wb.Styles[1].ForegroundColor = Color.Yellow;
                    wb.Styles[1].Pattern = BackgroundType.Solid;
                    wb.Styles[1].Font.IsBold = false;
                    wb.Styles[1].Borders[BorderType.TopBorder].Color = Color.Black;
                    wb.Styles[1].Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles[1].Borders[BorderType.BottomBorder].Color = Color.Black;
                    wb.Styles[1].Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles[1].Borders[BorderType.LeftBorder].Color = Color.Black;
                    wb.Styles[1].Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles[1].Borders[BorderType.RightBorder].Color = Color.Black;
                    wb.Styles[1].Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles.Add();
                    wb.Styles[2].VerticalAlignment = TextAlignmentType.Center;
                    wb.Styles[2].HorizontalAlignment = TextAlignmentType.Left;
                    wb.Styles[2].Font.Size = 10;
                    wb.Styles[2].Font.Name = "宋体";
                    wb.Styles[2].Font.IsBold = false;
                    wb.Styles[2].Borders[BorderType.TopBorder].Color = Color.Black;
                    wb.Styles[2].Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles[2].Borders[BorderType.BottomBorder].Color = Color.Black;
                    wb.Styles[2].Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles[2].Borders[BorderType.LeftBorder].Color = Color.Black;
                    wb.Styles[2].Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
                    wb.Styles[2].Borders[BorderType.RightBorder].Color = Color.Black;
                    wb.Styles[2].Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
                    #endregion

                    #region "导出报表"

                    foreach (export ex in list)
                    {
                        Worksheet wr = wb.Worksheets[intSeet];
                        wr.Name = ex.Name;
                        for (int i = 0; i < ex.Ary.Count; i++)
                        {
                            wr.Cells[0, i].PutValue(ex.Ary[i].ToString());
                            wr.Cells[0, i].Style = wb.Styles[0];
                            wr.Cells.SetColumnWidth(i, 30);
                        }
                        for (int i = 0; i < ex.Dt.Rows.Count; i++)
                        {
                            for (int j = 0; j < ex.Ary.Count; j++)
                            {
                                wr.Cells[i + 1, j].PutValue(ex.Dt.Rows[i][j].ToString().Equals("") ? "未明确" : ex.Dt.Rows[i][j].ToString());
                            }
                            wr.Cells.CreateRange(i + 1, 0, 1, ex.Ary.Count).Style = wb.Styles[2];
                        }
                        if (intSeet < list.Count - 1)
                            intSeet = wb.Worksheets.Add(SheetType.Worksheet);
                    }
                    wb.Save(path);

                    #endregion
                    return true;
                }
                return false;
            }
            catch { return false; }
        }

    }

    public class export
    {
        public String Name { get; set; }
        public ArrayList Ary { get; set; }
        public DataTable Dt { get; set; }
    }
}
